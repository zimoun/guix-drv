;;; Guix-Drv-Show --- Display Derivation.
;;; Copyright © 2024 Simon Tournier <simon.tournier@u-paris.fr>
;;;
;;; This file is NOT part of GNU Guix.  It is an extension.
;;;
;;; GNU Guix and this extension Guix-Drv-Show are free software; you can
;;; redistribute it and/or modify it under the terms of the GNU General Public
;;; License as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; GNU Guix and this extension Guix-Drv-Show are distributed in the hope that
;;; it will be useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix extensions drv-show)
  #:use-module (guix ui)                ;with-error-handling
  #:use-module (guix scripts)
  #:use-module (guix derivations)
  #:use-module (guix base32)
  #:use-module (srfi srfi-1)            ;list
  #:use-module (srfi srfi-26)           ;cut
  #:use-module (srfi srfi-37)           ;option
  #:use-module (ice-9 match)
  #:export (derivation->recutils
            guix-drv-show))

;;; Commentary:
;;;
;;;
;;; Code:

(define (show-help)
  (display (G_ "Usage: guix drv-show DERIVATION-STORE-PATH...
Display Guix derivation formatted with `recutils' package.

    guix drv-show /gnu/store/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-foobar.drv | recsel -P inputs"))
  (newline)

  (display (G_ "
  -h, --help             display this help and exit"))
  (display (G_ "
  -V, --version          display version information and exit"))
  (newline)
  (show-bug-report-information))

(define %options
  (list
   (option '(#\h "help") #f #f
           (lambda args
             (show-help)
             (exit 0)))
   (option '(#\V "version") #f #f
           (lambda args
             (show-version-and-exit "Drv extension of")))))



(define (derivation->recutils drv port)
  "Display derivation DRV."

  (define (max-path xputs get-path)
    (unless (null? xputs)
      (apply max
             (map (compose string-length
                           get-path)
                  xputs))))

  (define (path+out->string path out max)
    (let* ((len (string-length path))
           (npad (- (+ max 3) len))
           (pad (fold (lambda (x r)
                        (string-append " " r))
                      ""
                      (iota npad))))
      (string-append "+ " path pad "[" out "]")))

  (define (output->string output max)
    (match output
      ((out . drv)
       (let ((path (derivation-output-path drv)))
         (path+out->string path out max)))))

  (define (input->string input max)
    (let* ((path (derivation-input-path input))
           (out (string-join (derivation-input-sub-derivations input) ",")))
      (path+out->string path out max)))

  (define (builder-args->string args)
    (let* ((args (reverse args))
           (init (string-append "+ " (car args)))
           (rest (cdr args)))
      (fold (lambda (item res)
              (match item
                ("--no-auto-compile"
                 (string-append "+ --no-auto-compile\n" res))
                ("-C"
                 (string-append "+ -C " res))
                ("-L"
                 (string-append "+ -L " res))
                (path
                 (string-append path "\n" res))))
            init
            rest)))

    (define environment->string
    (match-lambda
      ((name . value)
       (string-append "+ " name ": " (format #f"~a" value)))))

  (let* ((outputs (derivation-outputs drv))
         (inputs (derivation-inputs drv))
         (system (derivation-system drv))
         (builder (derivation-builder drv))
         (builder-args (derivation-builder-arguments drv))
         (env-vars (derivation-builder-environment-vars drv))
         )
    (format port "name: ~a~%" (derivation-file-name drv))
    (format port "outputs:~%~{~a~%~}" (map (cute output->string <>
                                                 (max-path outputs
                                                           (compose derivation-output-path
                                                                    (match-lambda ((out . drv) drv)))))
                                           outputs))
    (when (fixed-output-derivation? drv)
      (format port "hash: ~a~%" (match outputs
                                  ((("out" . d))
                                   (bytevector->nix-base32-string
                                    (derivation-output-hash d))))))
    (format port "inputs:~%~{~a~%~}" (map (cute input->string <>
                                                (max-path inputs
                                                          derivation-input-path))
                                          inputs))
    (format port "system: ~a~%" system)
    (format port "builder:~%+ ~a~%" builder)
    (unless (null? builder-args)
      (format port "~a~%" (builder-args->string builder-args)))
    (format port "environment:~%~{~a~%~}" (map environment->string env-vars)))

  (format port "~%"))


(define-command (guix-drv-show . args)
  (category extension)
  (synopsis "display derivation")

  (with-error-handling
    (let* ((opts (reverse
                  (parse-command-line args %options
                                      (list '())
                                      #:build-options? #f)))
           (drvs (filter-map (lambda (path)
                               (catch #true
                                 (lambda ()
                                   (read-derivation-from-file path))
                                 (lambda (k . v)
                                   (warning (G_ "Not a valid derivation: ~a~%") path)
                                   #false)))
                             (delete-duplicates
                              (filter-map (match-lambda
                                            (('argument . path)
                                             path)
                                            (_ #false))
                                          opts)
                              string=?))))
      (for-each (lambda (drv)
                  (derivation->recutils drv (current-output-port)))
                drvs))
    #t))
