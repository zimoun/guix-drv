;;; Guix-Drv-Drv --- Transform Fixed-output Derivation.
;;; Copyright © 2024 Simon Tournier <simon.tournier@u-paris.fr>
;;;
;;; This file is NOT part of GNU Guix.  It is an extension.
;;;
;;; GNU Guix and this extension Guix-Drv-Drv are free software; you can
;;; redistribute it and/or modify it under the terms of the GNU General Public
;;; License as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; GNU Guix and this extension Guix-Drv-Drv are distributed in the hope that
;;; it will be useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix extensions drv-drv)
  #:use-module (guix ui)                ;with-error-handling
  #:use-module (guix scripts)
  #:use-module (guix derivations)
  #:use-module (guix store)
  #:use-module ((guix packages) #:select (default-guile))
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix hg-download)
  #:use-module (guix svn-download)
  #:use-module (srfi srfi-1)            ;list
  #:use-module (srfi srfi-11)           ;values
  #:use-module (srfi srfi-26)           ;cut
  #:use-module (srfi srfi-37)           ;option
  #:use-module (ice-9 match)
  #:use-module ((ice-9 string-fun) #:select (string-replace-substring))
  #:export (derivation->derivation
            guix-drv-drv))

;;; Commentary:
;;;
;;;
;;; Code:

(define (show-help)
  (display (G_ "Usage: guix drv2drv [OPTION]... DERIVATION-STORE-PATH
Transform (old) fixed-output derivation to (new) fixed-output derviation.

    guix drv2drv /gnu/store/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-foobar.drv"))
  (newline)

  (display (G_ "
  -h, --help             display this help and exit"))
  (display (G_ "
  -V, --version          display version information and exit"))
  (newline)
  (show-bug-report-information))

(define %options
  (list
   (option '(#\h "help") #f #f
           (lambda args
             (show-help)
             (exit 0)))
   (option '(#\V "version") #f #f
           (lambda args
             (show-version-and-exit "Drv extension of")))))


(define (list-or lst)
  (let loop ((lst lst))
    (match lst
      (() #false)
      ((head . tail)
       (or head (loop tail))))))


(define (fix-bioconductor url)
  (match (string-split url #\/)
    ((https "" "bioconductor.org" "packages" version "bioc" "src" "contrib" "Archive" name)
     (list
      (string-append "https://bioconductor.org/packages/" version
                     "/bioc/src/contrib/" name)
      url))
    ((https "" "bioconductor.org" "packages" version "data" type "src" "contrib" "Archive" name)
     (list
      (string-append "https://bioconductor.org/packages/" version
                     "/data/" type "/src/contrib/" name)
      url))
    (_
     (list url))))


(define-macro (extract-arguments symb-proc)
  `(lambda (sexp)
    (let loop ((sexp sexp))
      (match sexp
        ((or (? symbol? _)
             (? number? _)
             (? string? _)
             (? boolean? _)
             (? keyword? _)) #f)
        (() #f)
        ((,symb-proc . args)
         (if (null? args)
             #f
             args))
        ((head . rest)
         (or (loop head)
             (loop rest)))))))

(define (file->sexps file-as-str)
  (let loop ((port (open-input-file file-as-str))
             (sexps '()))
    (let ((sexp (read port)))
      (if (eof-object? sexp)
          sexps
          (loop port
                (cons sexp sexps))))))


(define (extract-svn sexp-builder)
  (define extract-svn-arguments
    (extract-arguments 'svn-fetch))

  (define (extract-value keyword lst)
    (match (member keyword lst)
      ((kw value . rest)
       value)
      (_ #false)))

  (define extract-values
    (match-lambda
      ((url revision _  . rest)
       (begin
         (values
          (match url
            (('string-append ('quote (? string? s)) rest ...) s)
            ((? string? s) s)
            (_ #false))
          (match revision
            ((? number? n) n)
            (('quote (? number? n)) n)
            (('quote n) n)
            (_ #false))
          (extract-value #:user-name rest)
          (extract-value #:recursive? rest)
          (extract-value #:password rest))))
      (_
       (values #f #f
               #f #f #f))))

  (define extract-locations
    (match-lambda
      ;; (((_ . (_ . ((_ _ ('quote locations)) . _))) . _)
      ;;  locations)
      (((_ _ (_ _ ('quote (? list? locations))) . _) . _)
       locations)
      (_ #false)))

  (let-values (((url revision user-name recursive? password)
                    (extract-values
                     (extract-svn-arguments sexp-builder))))
    (let ((locations (extract-locations sexp-builder)))
      (if url
          (if revision
              (values
               url revision
               locations
               user-name recursive? password)
              (error (G_ "SVN: cannot extract revision")))
        (error (G_ "SVN: cannot extract url"))))))


(define (extract-hg sexp-builder)
  (define extract-hg-arguments
    (extract-arguments 'hg-fetch))

  (define extract-values
    (match-lambda
      ((url revision . rest)
       (values
        (match url
          (('quote (? string? s)) s)
          ((? string? s) s)
          (_ #false))
        (match revision
          (('quote (? string? s)) s)
          ((? string? s) s)
          (_ #false))))
      (_
       (values #f #f))))

  (let-values (((url revision)
                (extract-values
                 (extract-hg-arguments sexp-builder))))
    (if url
        (if revision
            (values url revision)
            (error (G_ "HG: cannot extract revision")))
        (error (G_ "HG: cannot extract url")))))


(define (derivation->derivation drv)
  "Transform fixed-output derivation to recent one."
  (let* ((file-name (derivation-file-name drv))
         (name.drv (store-path-package-name file-name))
         (name (basename name.drv ".drv"))

         (system (derivation-system drv))

         (output (match (derivation-outputs drv)
                   ((("out" . drv-out))
                    drv-out)))
         (hash (derivation-output-hash output))
         (hash-algo (derivation-output-hash-algo output))
         (recursive? (derivation-output-recursive? output))

         (env-vars (derivation-builder-environment-vars drv))

         (get-value (lambda (str)
                      (match (assoc str env-vars)
                        ((_ . (or 0 "0")) 0)
                        ((_ . (or 1 "1")) 1)
                        ((_ . (or #f "#f" "#false")) #f)
                        ((_ . (or #t "#t" "#true")) #t)
                        ((_ . value)
                         (let ((value (string-replace-substring value "\"" "")))
                           (if (string-contains value "(")
                               (let* ((value (string-replace-substring value "(" ""))
                                      (value (string-replace-substring value ")" "")))
                                 (string-split value #\space))
                               value)))
                        (_ #false))))

         (fix-it (lambda (url procs)
                   (match url
                     ((? string? url)
                      (match (append-map (lambda (p) (p url)) procs)
                        ((x) x)
                        (xs xs)))
                     ((? list? urls)
                      (append-map (lambda (url)
                                    (append-map (lambda (p) (p url)) procs))
                                  urls))))))

         (cond
          ((string=? "builtin:download"
                     (derivation-builder drv))
           (let* ((url (get-value "url"))
                  (url (fix-it url (list
                                    fix-bioconductor)))
                  (executable? (get-value "executable"))
                  (drv (run-with-store (open-connection)
                         (url-fetch url
                                    hash-algo hash
                                    name
                                    #:system system
                                    #:guile (default-guile)
                                    #:executable? executable?))))
             drv))

          ((string=? "builtin:git-download"
                     (derivation-builder drv))
           (let* ((url (get-value "url"))
                  (commit (get-value "commit"))
                  (recursive? (get-value "recursive?"))
                  (module (resolve-interface '(gnu packages version-control)))
                  (git (module-ref module 'git-minimal))
                  (drv (run-with-store (open-connection)
                         (git-fetch (git-reference (url url)
                                                   (commit commit)
                                                   (recursive? recursive?))
                                    hash-algo hash
                                    name
                                    #:system system
                                    #:guile (default-guile)
                                    #:git git))))
             drv))

          ((assoc "git url" env-vars)
           ;; XXXX: LFS
           (let* ((url (get-value "git url"))
                  (commit (get-value "git commit"))
                  (recursive? (get-value "git recursive?"))
                  (module (resolve-interface '(gnu packages version-control)))
                  (git (module-ref module 'git-minimal))
                  (drv (run-with-store (open-connection)
                         (git-fetch (git-reference (url url)
                                                   (commit commit)
                                                   (recursive? recursive?))
                                    hash-algo hash
                                    name
                                    #:system system
                                    #:guile (default-guile)
                                    #:git git))))
             drv))

          ((assoc "svn url" env-vars)
           (let* ((url (get-value "svn url"))
                  (revision (string->number
                             (get-value "svn revision")))
                  (user-name (get-value "svn user name"))
                  (password (get-value "svn password"))
                  (locations (get-value "svn locations"))
                  (recursive? (get-value "svn recursive?"))
                  (module (resolve-interface '(gnu packages version-control)))
                  (svn (module-ref module 'subversion))
                  (drv (run-with-store (open-connection)
                         (if locations
                             (svn-multi-fetch (svn-multi-reference (url url)
                                                                   (revision revision)
                                                                   (user-name user-name)
                                                                   (password password)
                                                                   (locations locations)
                                                                   (recursive? recursive?))
                                              hash-algo hash
                                              name
                                              #:system system
                                              #:guile (default-guile)
                                              #:svn svn)
                             (svn-fetch (svn-reference (url url)
                                                       (revision revision)
                                                       (user-name user-name)
                                                       (password password))
                                        hash-algo hash
                                        name
                                        #:system system
                                        #:guile (default-guile)
                                        #:svn svn)))))
             drv))
          ((list-or
            (map (compose
                  (cute string-contains <> "subversion")
                  derivation-input-path)
                 (derivation-inputs drv)))
           (let-values (((url revision locations
                              user-name recursive? password)
                         (extract-svn
                          (file->sexps
                           (last
                            (derivation-builder-arguments drv))))))
             (let* ((module (resolve-interface '(gnu packages version-control)))
                    (svn (module-ref module 'subversion))
                    (drv (run-with-store (open-connection)
                           (if locations
                               (svn-multi-fetch (svn-multi-reference (url url)
                                                                     (revision revision)
                                                                     (user-name user-name)
                                                                     (password password)
                                                                     (locations locations)
                                                                     (recursive? recursive?))
                                                hash-algo hash
                                                name
                                                #:system system
                                                #:guile (default-guile)
                                                #:svn svn)
                               (svn-fetch (svn-reference (url url)
                                                         (revision revision)
                                                         (user-name user-name)
                                                         (password password))
                                          hash-algo hash
                                          name
                                          #:system system
                                          #:guile (default-guile)
                                          #:svn svn)))))
               drv)))

          ((list-or
            (map (compose
                  (cute string-contains <> "mercurial")
                  derivation-input-path)
                 (derivation-inputs drv)))
           (let-values (((url revision)
                         (extract-hg
                          (file->sexps
                           (last
                            (derivation-builder-arguments drv))))))
             (let* ((module (resolve-interface '(gnu packages version-control)))
                    (hg (module-ref module 'mercurial))
                    (drv (run-with-store (open-connection)
                           (hg-fetch (hg-reference
                                      (url url)
                                      (changeset revision))
                                     hash-algo hash
                                     name
                                     #:system system
                                     #:guile (default-guile)
                                     #:hg hg))))
               drv)))

          (else
           drv))))


(define (show drv)
  (format (current-output-port) "~a~%"
                      (derivation-file-name drv)))


(define-command (guix-drv-drv . args)
  (category extension)
  (synopsis "transform old fixed-output derivation")

  (with-error-handling
    (let* ((opts (reverse
                  (parse-command-line args %options
                                      (list '())
                                      #:build-options? #f)))
           (drvs (filter-map (lambda (path)
                               (catch #true
                                 (lambda ()
                                   (let ((drv (read-derivation-from-file path)))
                                     (if (fixed-output-derivation? drv)
                                         drv
                                         (begin
                                           (warning (G_ "Not a fixed-output derivation: ~a~%")
                                                    (derivation-file-name drv))
                                           #false))))
                                 (lambda (k . v)
                                   (warning (G_ "Not a valid derivation: ~a~%") path)
                                   #false)))
                             (delete-duplicates
                              (filter-map (match-lambda
                                            (('argument . path)
                                             path)
                                            (_ #false))
                                          opts)
                              string=?))))
      (for-each
       (compose show derivation->derivation)
       drvs))
    #t))
